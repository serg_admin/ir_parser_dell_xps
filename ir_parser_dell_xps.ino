/*
  Parser for remote control of dell XPS M1330
 */

#define TIME_KVANT 450.0  //Time of one bit in the IR package

unsigned long timePrev = 0;
unsigned long timeNow = 0;
unsigned int period = 0; //count tacts of last level
unsigned int pos_pos = 0; //currend position in the packet
volatile int sensorVal = 0; //level of ir sensor
int prevSensorVal = 0;  // prevois level of ir sensor
int long rezult = 0; //button code

void setup(){
  //start serial connection
  Serial.begin(115200);
  //configure pin2 as an input and enable the internal pull-up resistor
  pinMode(2, INPUT_PULLUP);
  pinMode(13, OUTPUT); 
  attachInterrupt(1, check, CHANGE);
}

void check()
{
  sensorVal = digitalRead(2);
  timePrev = timeNow;
  timeNow = micros();
  period = round((timeNow - timePrev)/TIME_KVANT);
  
  if ((period > 1000) && (pos_pos != 1)) pos_pos = 0; // check for noise
  
  if ((period == 6) && (sensorVal == 1)) //start marker
  {
    pos_pos = 6;
    rezult = 0;
  } else
  
  if ((pos_pos > 5) && (pos_pos < 59)) //wait for char marker
  {
    pos_pos+=period;
  } else
  
  if (pos_pos == 59) //control marker for except noise
  {
    if ((period == 2) && (sensorVal == 1)) 
    {
      pos_pos += period;
    }
    else pos_pos = 0;
  } else

  if ((pos_pos > 59) && (pos_pos < 69)) //wait for char marker
  {
    pos_pos+=period;
  } else
  
  if ((pos_pos >= 69) && (pos_pos < 85)) //calculate char
  {
    for(int a = 0; ((a < period) && (pos_pos < 85)); a++)
    {
      rezult = rezult << 1;
      rezult += sensorVal;
      pos_pos++;
    }
  } 
  if ((pos_pos >= 85) && (rezult > 0)) pos_pos = 1;
}

void loop(){

  if (pos_pos == 1)
  {
    switch (rezult)
    {
      case 44202 :
        Serial.println("sound+");
        break;
      case 44204 :
        Serial.println("sound-");
        break;
      case 43852 :
        Serial.println("mute");
        break;
      case 52522 :
        Serial.println("arr^");
        break;  
      case 52532 :
        Serial.println("arr>");
        break;
      case 52524 :
        Serial.println("arrv");
        break; 
      case 52530 :
        Serial.println("arr<");
        break;
      case 52554 :
        Serial.println("Ok");
        break;
      case 13002 :
        Serial.println("Backspace");
        break;
      case 19282 :
        Serial.println("Up");
        break;
      case 19284 :
        Serial.println("Down");
        break;
      case 45908 :
        Serial.println("back");
        break;
      case 45898 : 
        Serial.println("play/pause");
        break;
      case 45906 :
        Serial.println("forward");
        break;
      case 45740 :
        Serial.println("previous");
        break;
      case 45738 :
        Serial.println("next");
        break;
      case 46252 :
        Serial.println("stop");
        break;

      default:
        Serial.println(rezult);
    }

    pos_pos = 0;
    rezult = 0;
  }
}



